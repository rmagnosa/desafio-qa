# language: pt

Funcionalidade: Enviar mensangem para um contato no Whatsapp
  Como um usuario 
  Posso enviar uma mensagem para um dos meus contatos

Contexto:
    Dado que eu esteja no app do Whatsapp

Cenário: Enviar uma mensagem para um contato novo
    Quando que eu clico em "Nova Conversa"
    E selecionar um contato novo na lista
    Então posso enviar uma mensagem para o novo contato com sucesso

Cenário: Enviar uma mensagem para um contato existente
    Quando selecionar um contato existente na lista
    Então posso enviar uma mensagem para o contato existente com sucesso.