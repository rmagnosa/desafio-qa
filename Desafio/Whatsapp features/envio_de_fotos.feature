# language: pt

Funcionalidade: Enviar fotos para um contato no Whatsapp
  Como um usuario 
  Posso enviar uma foto para um dos meus contatos

Cenário: Enviar uma foto para um contato
    Dado que eu esteja no app do Whatsapp
    Quando seleciono um contato na lista
    E clicar no "+"
    Então posso enviar uma foto da minha galeria com sucesso