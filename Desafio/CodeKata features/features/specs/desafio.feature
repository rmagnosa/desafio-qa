#language: pt
  
Funcionalidade: Kata09 - Back to the Checkout
  Como caixa de supermercado
  Quero visualizar o valor total da  compra
  Para informar o cliente o quanto ele deve pagar
  
Esquema do Cenario: Caixa de Supermercado
  Quando passo os <Produtos> no caixa
  Entao vejo o valor total da compra
  
  Exemplos:
      | Produtos    | 
      | "AAABABABADA" | 
      | "ACCABDA"   | 
      | "ADDBAABBA" | 