class Super_Mercado

   def total_produtos(texto)
       @contadorA = texto.count "A"
       @contadorB = texto.count "B"
       @contadorC = texto.count "C"
       @contadorD = texto.count "D"
   end
   
   def tabela_de_precos()
        @A = (@contadorA / 3).floor * 130 + (@contadorA % 3) * 50
        @B = (@contadorB / 2).floor * 45 + (@contadorB % 2) * 30
        @C = @contadorC * 20
        @D = @contadorD * 15
   end

   def preco_total()
        tabela_de_precos()
        @calc_total = @A + @B + @C + @D
        print "O total a pagar é de R$ " + '%.2f' % @calc_total.to_s + " pelos produtos "
   end
   
end