Quando("passo os {string} no caixa") do |produtos|
  @super_Mercado = Super_Mercado.new
  @super_Mercado.total_produtos(produtos)
end

Entao("vejo o valor total da compra") do
  @super_Mercado.preco_total()
end